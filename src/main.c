#define ALLOC_SIZE 993

#include "mem_internals.h"
#include "mem.h"

static struct block_header* block_get_header(void* contents) {
	return (struct block_header*)(((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static void print_test_result(bool status, char* message, size_t test_number) {
	if (!status)
		printf("TEST #%zu FAILED: %s\n", test_number, message);
	else
		printf("TEST #%zu PASSED: OK\n", test_number);
}

static void test_one_memory_init() {
	void* addr = heap_init(REGION_MIN_SIZE);
	if (!addr) {
		print_test_result(false, "heap initiation failed", 1);
		return;
	}
	void* block = _malloc(ALLOC_SIZE);
	if (!block) {
		print_test_result(false, "block initiation failed", 1);
		return;
	}
	_free(block);
	heap_term();
	print_test_result(true, 0, 1);
}

static void test_two_one_block_freed() {
	heap_init(REGION_MIN_SIZE);
	void* memory= _malloc(ALLOC_SIZE);
	_free(memory);
	struct block_header* block = block_get_header(memory);
	if (!block->is_free) {
		print_test_result(false, "block not freed", 2);
		heap_term();
		return;
	}
	heap_term();
	print_test_result(true, 0, 2);
}

static void test_three_two_blocks_freed() {
	heap_init(REGION_MIN_SIZE);
	void* first_memory = _malloc(ALLOC_SIZE);
	void* second_memory = _malloc(ALLOC_SIZE);
	_free(first_memory);
	_free(second_memory);
	struct block_header* first_block = block_get_header(first_memory);
	struct block_header* second_block = block_get_header(second_memory);
	if (!first_block->is_free || !second_block->is_free) {
		print_test_result(false, "blocks not freed", 3);
		heap_term();
		return;
	}
	if (first_block->next != second_block) {
		print_test_result(false, "first block doesn't point to the second block", 3);
		heap_term();
		return;
	}
	heap_term();
	print_test_result(true, 0, 3);
}

static void test_four_memory_extended() {
	heap_init(REGION_MIN_SIZE);
	void* memory = _malloc(REGION_MIN_SIZE + ALLOC_SIZE);
	if (memory != HEAP_START + offsetof(struct block_header, contents)) {
		print_test_result(false, "memory not extends", 4);
		heap_term();
		return;
	}
	heap_term();
	print_test_result(true, 0, 4);
}

static void test_five_memory_extended_in_another_place() {
	heap_init(REGION_MIN_SIZE);
	void* pointer = mmap(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
	if (!pointer) {
		print_test_result(false, "memory after heap is occupied", 5);
		heap_term();
		return;
	}
	_malloc(REGION_MIN_SIZE + 1);
	heap_term();
	print_test_result(true, 0, 5);
}
int main() {
	test_one_memory_init();
	test_two_one_block_freed();
	test_three_two_blocks_freed();
	test_four_memory_extended();
	test_five_memory_extended_in_another_place();
	return 0;
}
